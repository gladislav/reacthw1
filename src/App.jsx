import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import React from 'react';
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import './App.css'

// class App extends React.Component {
//   state = {
//     isModal1Open: false,
//     isModal2Open: false,
//   };

//   toggleModal1 = () => {
//     this.setState((prevState) => ({ isModal1Open: !prevState.isModal1Open }));
//   };

//   toggleModal2 = () => {
//     this.setState((prevState) => ({ isModal2Open: !prevState.isModal2Open }));
//   };

//   render() {
//     return (
//       <div className="app">
//         <Button
//           backgroundColor="blue"
//           text="Open first modal"
//           onClick={this.toggleModal1}
//         />
//         <Button
//           backgroundColor="green"
//           text="Open second modal"
//           onClick={this.toggleModal2}
//         />

//         {this.state.isModal1Open && (
//           <Modal
//             header="Modal 1"
//             closeButton={true}
//             text="First window."
//             actions={<button onClick={this.toggleModal1}>Close</button>}
//           />
//         )}

//         {this.state.isModal2Open && (
//           <Modal
//             header="Modal 2"
//             closeButton={true}
//             text="Second window."
//             actions={<button onClick={this.toggleModal2}>Close</button>}
//           />
//         )}
//       </div>
//     );
//   }
// }

// export default App


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal1Open: false,
      isModal2Open: false,
    };
  }

  openModal1 = () => {
    this.setState({ isModal1Open: true });
  };

  closeModal1 = () => {
    this.setState({ isModal1Open: false });
  };

  openModal2 = () => {
    this.setState({ isModal2Open: true });
  };

  closeModal2 = () => {
    this.setState({ isModal2Open: false });
  };

  render() {
    const { isModal1Open, isModal2Open } = this.state;

    return (
      <div className="App">
        <Button
          backgroundColor="blue"
          text="Open first modal"
          onClick={this.openModal1}
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          onClick={this.openModal2}
        />

        {isModal1Open && (
          <Modal
            header="Modal 1"
            closeButton={true}
            text="First is the content of Modal 1."
            actions={<button onClick={this.closeModal1}>Close</button>}
            onClose={this.closeModal1}
          />
        )}

        {isModal2Open && (
          <Modal
            header="Modal 2"
            closeButton={true}
            text="Second content of Modal 2."
            actions={<button onClick={this.closeModal2}>Close</button>}
            onClose={this.closeModal2}
          />
        )}
      </div>
    );
  }
}

export default App;
