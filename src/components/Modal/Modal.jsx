import React from 'react';
import './Modal.scss';

class Modal extends React.Component {
  render() {
    const { header, closeButton, text, actions } = this.props;

    return (
      <div className="modal">
        <div className="modal-content">
          <div className="modal-header">
            <h2>{header}</h2>
            {closeButton && <span className="close-button">×</span>}
          </div>
          <div className="modal-body">
            <p>{text}</p>
          </div>
          <div className="modal-footer">{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
